import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms'
import {DragDropModule} from '@angular/cdk/drag-drop'
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'
import { RegistrationComponent } from './components/registration/registration.component';
import { LoginComponent } from './components/login/login.component';
import { UsersComponent } from './components/users/users.component';
import { GalleryComponent } from './components/gallery/gallery.component';
import { TokenInterceptorService } from './services/token-interceptor.service';
import { UserDetailsComponent } from './components/user-details/user-details.component';
import { GalleryDetailsComponent } from './components/gallery-details/gallery-details.component';
import { GalleryAdvancedComponent } from './components/gallery-advanced/gallery-advanced.component';

@NgModule({
  declarations: [
    AppComponent,
    RegistrationComponent,
    LoginComponent,
    UsersComponent,
    GalleryComponent,
    UserDetailsComponent,
    GalleryDetailsComponent,
    GalleryAdvancedComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    DragDropModule
  ],
  providers: [{provide: HTTP_INTERCEPTORS, useClass: TokenInterceptorService, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
