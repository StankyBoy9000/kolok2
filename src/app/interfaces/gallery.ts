export interface Gallery {
    id: number,
    name: string,
    type: string,
    directory: string,
    size: number
}
