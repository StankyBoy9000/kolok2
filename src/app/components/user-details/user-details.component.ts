import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/interfaces/user';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {

  constructor(private _userService: UserService, private route: ActivatedRoute, private router: Router) { }

  public user: User = {
    id: 0,
    username: "",
    password: "",
    roles: []
  };

  ngOnInit(): void {
    let id = parseInt(this.route.snapshot.paramMap.get('id'))
    this._userService.getUser(id).subscribe( data => this.user = data);
  }

  deleteUser(id){
    this._userService.deleteUser(id).subscribe(() => {
      this.router.navigate(["users"])
    });
  }

}
