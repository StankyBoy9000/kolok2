import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/interfaces/user';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  public users: User[] = []
  public selectedUser: User;



  public user: User = {
    id: 0,
    username: "",
    password: "",
    roles: []
  };
  constructor(private _userService: UserService, private router: Router) { }

  ngOnInit(): void {
    this.getUsers();
  }


  getUsers(){
    this._userService.getUsers().subscribe( data => this.users = data);
  }

  deleteUser(id){
    this._userService.deleteUser(id).subscribe(() => this.getUsers());
  }

  addUser(){
    console.log(this.user);
    if(this.checkIfUserExists(this.user.id)){
      this._userService.updateUser(this.user.id,this.user).subscribe(() => this.getUsers());
    }else{
      this._userService.addUser(this.user).subscribe(() => this.getUsers());
    }
  }

  checkIfUserExists(id){
    for(let i = 0; i < this.users.length; i++){
      if(id == this.users[i].id){
        return true;
      }
    }
    return false;
  }

  userDetails(user: User){
    console.log(user.id)
    this.router.navigate(['/users', user.id]);
  }

  logObject(){
    console.log(this.selectedUser);
  }
}
