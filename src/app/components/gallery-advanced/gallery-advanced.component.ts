import { Component, OnInit } from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { GalleryService } from 'src/app/services/gallery.service';
import { Router } from '@angular/router';
import { Gallery } from 'src/app/interfaces/gallery';

@Component({
  selector: 'app-gallery-advanced',
  templateUrl: './gallery-advanced.component.html',
  styleUrls: ['./gallery-advanced.component.css']
})
export class GalleryAdvancedComponent implements OnInit {

  public galleries = []
  public dir1 = []
  public dir2 = []
  public selectedGallery: Gallery;
  public gallery: Gallery = {
    id: 0,
    name: "",
    type: "",
    directory: "",
    size: 0
  };
  constructor(private _galleryService: GalleryService, private router: Router) { }

  ngOnInit(): void {
    this.getGalleries();
  }

  getGalleries(){
    this._galleryService.getGalleries().subscribe( data => {
      this.galleries = data;
      this.sortGallery();
    });
  }

  deleteGallery(id){
    this._galleryService.deleteGallery(id).subscribe(() => this.getGalleries());
  }

  sortGallery(){
    this.dir1 = []
    this.dir2 = []
    for(let i = 0; i < this.galleries.length; i++){
      if(this.galleries[i].directory == "d1"){
        this.dir1.push(this.galleries[i])
      } else{
        this.dir2.push(this.galleries[i])
      }
    }
  }

  addGallery(){
    console.log(this.gallery);
    if(this.checkIfGalleryExists(this.gallery.id)){
      this._galleryService.updateGallery(this.gallery.id,this.gallery).subscribe(() => this.getGalleries());
    }else{
      this._galleryService.addGallery(this.gallery).subscribe(() => this.getGalleries());
    }

    this.sortGallery()
  }

  checkIfGalleryExists(id){
    for(let i = 0; i < this.galleries.length; i++){
      if(id == this.galleries[i].id){
        return true;
      }
    }
    return false;
  }

  galleryDetails(gallery){
    this.router.navigate(['/gallery', gallery.id]);
  }

  logObject(){
    console.log(this.selectedGallery);
  }

  selectGallery(sGallery){
    this.selectGallery = sGallery;
  }

  onDrop(event: CdkDragDrop<string[]>){
    if(event.previousContainer === event.container){
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex); 
    } else{
      transferArrayItem(event.previousContainer.data,event.container.data,event.previousIndex,event.currentIndex);
    }
  }
}
