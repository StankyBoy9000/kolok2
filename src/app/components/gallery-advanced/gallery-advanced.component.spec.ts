import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GalleryAdvancedComponent } from './gallery-advanced.component';

describe('GalleryAdvancedComponent', () => {
  let component: GalleryAdvancedComponent;
  let fixture: ComponentFixture<GalleryAdvancedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GalleryAdvancedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GalleryAdvancedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
