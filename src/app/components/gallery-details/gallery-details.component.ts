import { Component, OnInit } from '@angular/core';
import { Gallery } from 'src/app/interfaces/gallery';
import { GalleryService } from 'src/app/services/gallery.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-gallery-details',
  templateUrl: './gallery-details.component.html',
  styleUrls: ['./gallery-details.component.css']
})
export class GalleryDetailsComponent implements OnInit {

  public gal: Gallery = {
    id: 0,
    name: "",
    type: "",
    directory: "",
    size: 0
  };
  constructor(private _galleryService: GalleryService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    let id = parseInt(this.route.snapshot.paramMap.get('id'))
    this._galleryService.getGallery(id).subscribe( data => this.gal = data);
  }
}
