import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/interfaces/user';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  id: number;
  username:string;
  password: string;
  role: string;

  public user: User = {
    id: 0,
    username: "",
    password: "",
    roles: []
    
  }
  possibleRoles = []
  constructor(private _registrationService: UserService) { }

  ngOnInit(): void {
  }

  register(){
    this.possibleRoles.push(this.role);
    this.user.id = this.id
    this.user.username = this.username
    this.user.password = this.password
    this.user.roles = this.possibleRoles

    this._registrationService.addUser(this.user).subscribe();

  }

}
