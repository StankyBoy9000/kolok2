import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TokenUtilsService } from './token-utils.service';


@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private user;
  private _url: string = 'http://localhost:3000/student'


  private user_token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwicGFzc3dvcmQiOiJhZG1pbiIsInJvbGVzIjpbIlJPTEVfVVNFUiJdfQ.7vRKKOfTNAWec1M7C2pDKz3F4seGVIvnTjkFOXnZaWU"
  private admin_token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Ik1hcmtvIFN0YW5vamV2aWMiLCJuYW1lIjoiTWFya28iLCJyb2xlcyI6IlJPTEVfQURNSU4ifQ.5qUCMDLJlcbhx00uwMsaN4RZzHe9iynkNRB5qaRRWXg"

  private token = this.admin_token
  constructor(private tokenUtils: TokenUtilsService,private http: HttpClient) { 
    this.user = tokenUtils.parseToken(this.token);
  }

  getLoggedInUser(){
    return this.user;
  }

  getToken(){
    return this.token;
  }

  login(username, password){
    // return this.http.get<User>(this._url+"/"+id);
  }
}