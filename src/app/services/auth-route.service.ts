import { Injectable } from '@angular/core';
import { CanActivate, Router, CanActivateChild } from '@angular/router';
import { LoginService } from './login.service';

@Injectable({
  providedIn: 'root'
})
export class AuthRouteService implements CanActivate, CanActivateChild {

  constructor(private loginService: LoginService, private router: Router) { }
  canActivateChild(childRoute: import("@angular/router").ActivatedRouteSnapshot, state: import("@angular/router").RouterStateSnapshot): boolean | import("@angular/router").UrlTree | import("rxjs").Observable<boolean | import("@angular/router").UrlTree> | Promise<boolean | import("@angular/router").UrlTree> {
    for(let i of childRoute.data.allowedRoles){
      console.log(i)
      if(i == this.loginService.getLoggedInUser().roles){
        console.log(this.loginService.getLoggedInUser().roles)
        return true; 
      }
    }
    console.log("went in false")
    this.router.navigate(["login"])
    return false;
  }


  canActivate(route: import("@angular/router").ActivatedRouteSnapshot, state: import("@angular/router").RouterStateSnapshot): boolean | import("@angular/router").UrlTree | import("rxjs").Observable<boolean | import("@angular/router").UrlTree> | Promise<boolean | import("@angular/router").UrlTree> {
    for(let i of route.data.allowedRoles){
      console.log(i)
      if(i == this.loginService.getLoggedInUser().roles){
        console.log(this.loginService.getLoggedInUser().roles)
        return true; 
      }
    }
    console.log("went in false")
    this.router.navigate(["login"])
    return false;
  }
}