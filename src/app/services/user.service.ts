import { Injectable } from '@angular/core';
import { User } from '../interfaces/user';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private _url: string = 'http://localhost:3000/users'

  constructor(private http: HttpClient) { }

  getUsers(): Observable<User[]>{
    return this.http.get<User[]>(this._url);
  }

  getUser(id): Observable<User>{
    return this.http.get<User>(this._url+"/"+id);
  }

  deleteUser(id): Observable<User>{
    return this.http.delete<User>(this._url+"/"+id);
  }

  addUser(user: User): Observable<User>{
    return this.http.post<User>(this._url,user);
  }

  updateUser(id, user): Observable<User>{
    return this.http.put<User>(`http://localhost:3000/student/${id}`, user);
  }
}
