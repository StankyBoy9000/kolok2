import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Gallery } from '../interfaces/gallery';

@Injectable({
  providedIn: 'root'
})
export class GalleryService {

  private _url: string = 'http://localhost:3000/gallery'

  constructor(private http: HttpClient) { }

  getGallery(id): Observable<Gallery>{
    return this.http.get<Gallery>(this._url+"/"+id);
  }

  addGallery( test: Gallery){
    return this.http.post<Gallery>(this._url,test);
  }

  deleteGallery(id): Observable<Gallery>{
    return this.http.delete<Gallery>(this._url+"/"+id);
  }

  updateGallery(id, gal): Observable<Gallery>{
    return this.http.put<Gallery>(`http://localhost:3000/gallery/${id}`, gal);
  }

  getGalleries(): Observable<Gallery[]>{
    return this.http.get<Gallery[]>(this._url);
  }
}
