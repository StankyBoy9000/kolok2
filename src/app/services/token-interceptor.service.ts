import { Injectable } from '@angular/core';
import { HttpInterceptor } from '@angular/common/http';
import { LoginService } from './login.service';

@Injectable()
export class TokenInterceptorService implements HttpInterceptor {

  constructor(private loginService: LoginService) { }
  intercept(req: import("@angular/common/http").HttpRequest<any>, next: import("@angular/common/http").HttpHandler): import("rxjs").Observable<import("@angular/common/http").HttpEvent<any>> {
    let newReq = req.clone({
      headers: req.headers.set("Authorization", this.loginService.getToken())
    });
    return next.handle(newReq);
  }
}
