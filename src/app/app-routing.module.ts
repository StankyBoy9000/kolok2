import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthRouteService } from './services/auth-route.service';
import { LoginComponent } from './components/login/login.component';
import { RegistrationComponent } from './components/registration/registration.component';
import { UsersComponent } from './components/users/users.component';
import { GalleryComponent } from './components/gallery/gallery.component';
import { UserDetailsComponent } from './components/user-details/user-details.component';
import { GalleryDetailsComponent } from './components/gallery-details/gallery-details.component';
import { GalleryAdvancedComponent } from './components/gallery-advanced/gallery-advanced.component';


const routes: Routes = [
  { path: '',
    canActivateChild: [AuthRouteService],
    data: {
      allowedRoles: []
    },
    children: [
      { path: 'users/:id',
       component: UserDetailsComponent,
       data:{
        allowedRoles: ["ROLE_ADMIN"]
        }
      },
      
      {
        path: "registration",
        component: RegistrationComponent,
        data:{
          allowedRoles: ["ROLE_ADMIN"]
        }
      }
      ,
      {
        path: "gallery/advanced",
        component: GalleryAdvancedComponent,
        data:{
          allowedRoles: ["ROLE_ADMIN"]
        }
      }
      ,
      {
        path: "users",
        component: UsersComponent,
        data:{
          allowedRoles: ["ROLE_USER","ROLE_ADMIN"]
        }
      }
    ]
  },
  {
    path: "login",
    component: LoginComponent
  },
  {
    path: "gallery",
    component: GalleryComponent
  },
  { path: 'gallery/:id',
    component: GalleryDetailsComponent,
  }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
